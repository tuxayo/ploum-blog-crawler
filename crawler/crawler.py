import urllib.request
import re
import os
import json

from django.conf import settings
from bs4 import BeautifulSoup
from whoosh.index import create_in
from whoosh.fields import Schema, TEXT, STORED, ID

BLOG_BASE_URL = 'https://ploum.net'


def crawl():
    soup = BeautifulSoup(_download_page(BLOG_BASE_URL), 'html.parser')
    posts_urls = _get_all_posts_urls(soup)

    _index_all_posts(posts_urls)


def _index_all_posts(posts_urls):
    print('indexing posts')

    index_writer = _create_whoosh_index().writer()
    number_of_posts = len(posts_urls)
    for number, post_url in enumerate(posts_urls):
        _index_post(post_url, index_writer)
        if number % 10 == 0:
            print(str(number+1) + '/' + str(number_of_posts))
    index_writer.commit()


def _create_whoosh_index():
    schema = Schema(url=ID(stored=True),
                    title=TEXT(stored=True),
                    links_to_other_posts=STORED,
                    content=TEXT(stored=True))
    index_path = os.path.join(settings.BASE_DIR,
                              'ploum_blog_crawler',
                              'whoosh_index')
    if not os.path.exists(index_path):
        os.mkdir(index_path)
    return create_in(index_path, schema)


def _index_post(post_url, index_writer):
    try:
        soup = BeautifulSoup(_download_page(post_url), 'html5lib')
    except urllib.error.HTTPError as e:
        if e.getcode() == 500:
            print('For some reason the server returned a 500 error although the link is valid')
            return
        else:
            raise e

    except urllib.error.URLError:
        print('urllib.error.URLError: network issue retrying')
        return _index_post(post_url, index_writer)

    title, content, links_to_other_blog_posts = _extract_post_data(soup)
    [post_cleaned_url] = _keep_links_to_other_posts([post_url])

    _index_post_data(index_writer,
                     title,
                     post_cleaned_url,
                     content,
                     links_to_other_blog_posts)


def _extract_post_data(soup):
    title = soup.select('[itemprop="name"]')[0]['content']
    content_tree = _extract_content_tree(soup)
    content_text = content_tree.text.strip()
    links_to_other_blog_posts = _extract_links_to_other_blog_posts(content_tree)

    return title, content_text, links_to_other_blog_posts


def _index_post_data(index_writer, title, url, content, links_to_other_posts):
    links_json = json.dumps(list(links_to_other_posts))
    index_writer.add_document(url=url,
                              title=title,
                              links_to_other_posts=links_json,
                              content=content)


def _extract_content_tree(soup):
    content = soup.select('.entry-content')[0]

    content.select('.readoffline-embed')[0].extract()  # remove ebook export links

    tag_end_actual_content = _find_end_actual_content(content)

    # Need to convert the generator to a list because when using extract() we
    # will alter the HTML tree which is the source of the generator. That will
    # cause the generator to empty prematurely and prevent us from removing the
    # nodes we want.
    tags_after_actual_content = list(tag_end_actual_content.parent.parent.next_siblings)
    for tag_to_remove in tags_after_actual_content:
        tag_to_remove.extract()
    tag_end_actual_content.extract()

    # now content should contain only the actual content
    return content


def _find_end_actual_content(content):
    tag_end_actual_content = content.select('p em small a')[0].parent
    assert tag_end_actual_content.text.startswith("Merci d'avoir pris le temps de lire")
    return tag_end_actual_content


def _extract_links_to_other_blog_posts(soup):
    link_tags_in_content = soup.find_all('a')
    links_in_content = [tag['href'] for tag in link_tags_in_content]
    return _keep_links_to_other_posts(links_in_content)


def _keep_links_to_other_posts(links):
    links_to_the_blog_itself = filter(_is_link_to_the_blog_itself, links)
    links_to_the_blog_itself = map(_remove_http_https_prefix,
                                   links_to_the_blog_itself)
    links_to_the_blog_itself = map(_remove_post_prefix,
                                   links_to_the_blog_itself)
    links_to_the_blog_itself = map(_remove_slash_suffix,
                                   links_to_the_blog_itself)
    links_to_the_blog_itself = map(_remove_date_prefix,
                                   links_to_the_blog_itself)

    links_to_other_posts = filter(_is_link_to_other_post,
                                  links_to_the_blog_itself)
    return links_to_other_posts


def _is_link_to_the_blog_itself(link):
    return (link.startswith('/') or
            link.startswith('https://ploum.net') or
            link.startswith('http://ploum.net'))


def _is_link_to_other_post(link):
    return (not link.startswith('/wp-content/') and
            not link.startswith('/images/') and
            not link.startswith('/textes/') and
            not link.startswith('/public/') and
            not link.startswith('/tag/') and
            not link.startswith('/gallery/') and
            not link.startswith('/feed/') and
            not link.startswith('/pages/') and
            not link.startswith('/rss.php') and
            not link == "/contact" and
            not link == "")


def _remove_http_https_prefix(link):
    if link.startswith('https://'):
        return link[17:]
    if link.startswith('http://'):
        return link[16:]
    return link


def _remove_post_prefix(link):
    if link.startswith('/post'):
        return link[5:]
    return link


def _remove_date_prefix(link):
    # '/2005/04/08/foo' → '/foo'
    return re.sub('\/\d\d\d\d\/\d\d\/\d\d', '', link)


def _remove_slash_suffix(link):
    if link.endswith('/'):
        return link[:-1]
    return link


def _get_number_of_posts(soup):
    [post_count_tag] = soup.select('.site-categories-count a')
    # example of tag content: '570 articles'
    post_count = post_count_tag.string.split(' ')[0]
    return post_count


def _get_all_posts_urls(soup):
    print('Colecting posts urls')
    number_of_posts = _get_number_of_posts(soup)
    all_posts_urls = _get_current_page_posts_urls(soup)

    # page 1 already done otherwise we would start at 0
    page_number = 1
    try:
        while True:
            page_number += 1
            page_url = BLOG_BASE_URL + '/page/' + str(page_number)
            soup = BeautifulSoup(_download_page(page_url), 'html.parser')
            current_page_posts_urls = _get_current_page_posts_urls(soup)
            all_posts_urls += current_page_posts_urls
            print(str(len(all_posts_urls)) + '/' + number_of_posts)
    except urllib.error.HTTPError as e:
        if e.getcode() == 404:
            return all_posts_urls
        else:
            raise e


def _get_current_page_posts_urls(soup):
    posts_link_tags = soup.select('.entry-title a')
    return [tag['href'] for tag in posts_link_tags]


def _download_page(url):
    # as a respectable person you should let the crawled website contact you
    contact = 'REPLACE ME WITH AN URL OR EMAIL ADDRESS'
    user_agent = 'crawler (contact: ' + contact + ') lib: Python-urllib'
    response = urllib.request.urlopen(
        urllib.request.Request(url, headers={'User-Agent': user_agent})
    )
    data = response.read()       # a `bytes` object
    html = data.decode('utf-8')  # an `str`
    return html
