from django.conf import settings
from django.core.management.base import BaseCommand

import crawler.crawler


class Command(BaseCommand):
    help = 'Crawl and index the blog'

    def handle(self, *args, **options):
        crawler.crawler.crawl()
