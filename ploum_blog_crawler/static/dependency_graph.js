// Based on https://bl.ocks.org/mbostock/1153292
// License: GNU GPLv3

"use strict";

var nodes = {};
var width = 1300,
    height = 780;
var force,
    path,
    circle,
    text;

d3.json("/posts/dependencies.json", drawGraph);

function drawGraph(error, data) {
    if (error) {
        console.error(error.responseURL + ' ' + error.statusText);
        return;
    }

    // Compute the distinct nodes from the links.
    data.forEach(function(link) {
        link.source = nodes[link.source] || (nodes[link.source] = {url: link.source});
        link.target = nodes[link.target] || (nodes[link.target] = {url: link.target});
    });

    force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(data)
        .size([width, height])
        .linkDistance(60)
        .charge(-300)
        .on("tick", tick)
        .start();
    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("xmlns:xlink", "http://www.w3.org/1999/xlink");
    // Per-type markers, as they don't inherit styles.
    svg.append("defs").selectAll("marker")
        .data(["marker"])
      .enter().append("marker")
        .attr("id", function(d) { return d; })
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 15)
        .attr("refY", -1.5)
        .attr("markerWidth", 6)
        .attr("markerHeight", 6)
        .attr("orient", "auto")
      .append("path")
        .attr("d", "M0,-5L10,0L0,5");
    path = svg.append("g").selectAll("path")
        .data(force.links())
      .enter().append("path")
        .attr("class", "link")
        .attr("marker-end", "url(#marker)");
    circle = svg.append("g").selectAll("circle")
        .data(force.nodes())
      .enter().append("circle")
        .attr("r", 6)
        .call(force.drag);
    text = svg.append("g").selectAll("text")
        .data(force.nodes())
      .enter().append("a")
        .attr("xlink:xlink:href", function(d) { return "https://ploum.net" + d.url; })
      .append("text")
        .attr("x", 8)
        .attr("y", ".31em")
        .text(function(d) { return d.url; });

}

// Use elliptical arc path segments to doubly-encode directionality.
function tick() {
  path.attr("d", linkArc);
  circle.attr("transform", transform);
  text.attr("transform", transform);
}
function linkArc(d) {
  var dx = d.target.x - d.source.x,
      dy = d.target.y - d.source.y,
      dr = Math.sqrt(dx * dx + dy * dy);
  return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
}
function transform(d) {
  return "translate(" + d.x + "," + d.y + ")";
}
