import os
import json
from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings
from whoosh.index import open_dir
from whoosh.qparser import QueryParser


def home(request):
    query_string = request.GET.get('q')
    if query_string is None:
        return render(request, 'home.html')

    ix = open_dir(os.path.join(settings.BASE_DIR,
                               'ploum_blog_crawler',
                               'whoosh_index'))
    query = QueryParser('content', ix.schema).parse(query_string)
    searcher = ix.searcher()
    results = searcher.search(query)

    urls = ['https://ploum.net' + res['url'] for res in results]
    titles = [res['title'] for res in results]
    content_previews = [res['content'][:120] + '…' for res in results]

    prepared_results = zip(titles, urls, content_previews)
    return render(request, 'home.html', {'results': prepared_results})


def dependency_graph(request):
    return render(request, 'dependency_graph.html')


def dependencies(request):
    dependencies = []
    index_path = os.path.join(settings.BASE_DIR,
                              'ploum_blog_crawler',
                              'whoosh_index')
    reader = open_dir(index_path).reader()
    for _, post in reader.iter_docs():
        links_json = post['links_to_other_posts']
        links = json.loads(links_json)
        links = filter(_remove_series, links)
        for link in links:
            dependencies.append({'source': post['url'], 'target': link})

    return JsonResponse(dependencies, safe=False)


def _remove_series(link):
    return not link.startswith('/series/')
